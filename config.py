root = "/path/to/documents/"
# The path to the direvtory containing the documents

title = "User-Visible Title"
# A user-visible title for the server (e.g. "School Documents" or "My Notes")

allowed_file_types = ["txt", "odt", "pdf", "md"]
# File types that LANdoc will serve; all other file types will result in 403
# Forbidden

# To disallow one of these file types, remove it; to allow another type, add
# its extension without the `.`

# `md` and `txt` files will be displayed in the LANdoc viewer; all other files
# will be sent as-is to the client
